import { Component } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Gestión de Reuniones';
  url = '';

constructor(private router: Router, private activatedRoute: ActivatedRoute) {
  this.router.events.subscribe(
    (event: any) => {
      if (event instanceof NavigationEnd) {
        this.url = this.router.url;
      }
    }
  );
}
}
