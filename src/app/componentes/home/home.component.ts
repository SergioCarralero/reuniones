import { Component, OnInit } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  public modoLectura() {
    environment.LECTURA = true;
    environment.ESCRITURA = false;
    this.irAReuniones();
  }

  public modoEscritura() {
    environment.LECTURA = false;
    environment.ESCRITURA = true;
    this.irAUsuarios();
  }

  public modoAdministrador() {
    this.router.navigate(['/usuarios/24/detalles']);
  }

  private irAUsuarios() {
    this.router.navigate(['/usuarios']);
  }

  private irAReuniones() {
    this.router.navigate(['/reuniones']);
  }
}
