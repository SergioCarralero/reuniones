import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { environment } from '../../../../environments/environment';
import { Router, NavigationEnd } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Usuario } from '../../../models/usuario';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  public usuario = environment.USUARIO;
  private url: string;

  constructor(private location: Location, private router: Router, private toast: ToastrService) {
    this.router.events.subscribe(
      (event: any) => {
        if (event instanceof NavigationEnd) {
          this.url = this.router.url;
        }
      }
    );
  }

  ngOnInit() {
  }

  public irAtras() {
    this.location.back();
  }

  public irAlante() {
    window.location.reload();
  }

  public irAReuniones() {
    this.router.navigate(['/reuniones']);
  }

  public irAMisReuniones() {
    this.checkUsuario();
    if (this.usuario.id === 0) {
      this.router.navigate(['/usuarios']);
      this.toast.warning('Primero debes iniciar sesión', 'Log in', { timeOut: 3000, progressAnimation: 'increasing', progressBar: true });
    } else if (this.usuario.nombre !== 'Administrador') {
      this.router.navigate([`/usuarios/${this.usuario.id}/reuniones`]);
    } else {
      // tslint:disable-next-line: max-line-length
      this.toast.warning('El administrador no tiene reuniones', 'Administrador', { timeOut: 3000, progressAnimation: 'increasing', progressBar: true });
    }
  }

  public editarPerfil() {
    this.checkUsuario();
    if (this.usuario.id === 0) {
      this.router.navigate(['/usuarios']);
      this.toast.warning('Primero debes iniciar sesión', 'Log in', { timeOut: 3000, progressAnimation: 'increasing', progressBar: true });
    } else {
      this.router.navigate([`/usuarios/${this.usuario.id}/editarperfil`]);
    }
  }

  public logIn() {
    this.router.navigate(['/usuarios']);
  }

  public cambiarDeCuenta() {
    Swal.fire({
      title: 'Cambiar de Cuenta',
      // tslint:disable-next-line: max-line-length
      text: `Se cerrará la sesión actual, ¿Continuar?`,
      type: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Si'
    }).then((result) => {
      if (result.value) {
        environment.USUARIO = new Usuario();
        if (this.url === '/usuarios') {
          location.reload();
        } else {
          this.router.navigate(['/usuarios']);
        }
      }
    });
  }

  public checkUsuario() {
    this.usuario = environment.USUARIO;
  }

  public irATipos() {
    this.router.navigate([`/tipos`]);
  }

  public irAUbicaciones() {
    this.router.navigate([`/ubicaciones`]);
  }

  public irAUsuarios() {
    this.router.navigate(['/usuarios']);
  }

  public logOut() {
    this.checkUsuario();
    if (this.usuario.id !== 0) {
      Swal.fire({
        title: 'Cerrar Sesión',
        // tslint:disable-next-line: max-line-length
        text: `Se cerrará la sesión actual, ¿Continuar?`,
        type: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Si'
      }).then((result) => {
        if (result.value) {
          environment.USUARIO = new Usuario();
          this.router.navigate(['/home']);
        }
      });
    } else {
      Swal.fire({
        title: 'Salir de la aplicación',
        // tslint:disable-next-line: max-line-length
        text: `Se cerrará la aplicación, ¿Continuar?`,
        type: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Si'
      }).then((result) => {
        if (result.value) {
          this.router.navigate(['/home']);
        }
      });
    }
  }
}
