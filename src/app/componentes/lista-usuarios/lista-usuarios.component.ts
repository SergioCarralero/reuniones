import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/app/models/usuario';
import { Router } from '@angular/router';
import { UsuariosService } from '../../services/usuarios.service';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../../environments/environment';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-lista-usuarios',
  templateUrl: './lista-usuarios.component.html',
  styleUrls: ['./lista-usuarios.component.css']
})
export class ListaUsuariosComponent implements OnInit {


  listaUsuarios: Usuario[];

  public usuario = environment.USUARIO;

  constructor(private usuarioService: UsuariosService, private router: Router,
              private toast: ToastrService) { }

  ngOnInit() {
    this.getUsuarios();
  }

  private getUsuarios() {
    this.usuarioService.GetUsuariosList().subscribe((data: Usuario[]) => {
      this.listaUsuarios = data;
      for (let i = 0; i < data.length; i++) {
        this.listaUsuarios[i].avatar = '../../assets/images/' + data[i].avatar;
      }
      console.log(this.listaUsuarios);
    }, error => {
// tslint:disable-next-line: max-line-length
      this.toast.error('Error de conexion comprueba que la api este arrancada y funcionando correctamente', 'Error', {timeOut: 3000, progressBar: true });
    });
  }

  public logIn(id: number) {
    this.router.navigate([`usuarios/${id}/detalles`]);
  }

  public darDeAlta() {
    this.router.navigate([`usuarios/alta`]);
  }

  public eliminarUsuario(id: number) {
    Swal.fire({
      title: 'Eliminar Regsitro',
      text: `¿Estas seguro@ de que quieres borrar a este usuario?`,
      type: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Si'
    }).then((result) => {
      if (result.value) {
        Swal.fire({
          title: '¿Seguro?',
          text: `Se borraran todas las reuniones credas por este usuario.`,
          type: 'question',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText: 'Cancelar',
          confirmButtonText: 'Si'
        }).then((res) => {
          if (res.value) {
            this.usuarioService.EliminarUsuario(id).subscribe(_ => {
              Swal.fire({
                type: 'success',
                title: 'Usuari@ Eliminado',
                text: 'El usaurio ha sido eliminado con exito',
                showConfirmButton: true,
                timer: 1000
              });
              this.getUsuarios();
            });
          }
        });
      }
    });
  }
}
