import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Usuario } from 'src/app/models/usuario';
import Swal from 'sweetalert2';
import { UsuariosService } from '../../services/usuarios.service';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-detalles-usuario',
  templateUrl: './detalles-usuario.component.html',
  styleUrls: ['./detalles-usuario.component.css']
})
export class DetallesUsuarioComponent implements OnInit {

  idUsuario;
  usuario: Usuario = new Usuario();
  contrsena;
  nombre;
  avatar: string;
  profesion: string;

  constructor(private usuarioService: UsuariosService, private activatedRoute: ActivatedRoute, private router: Router) {
    this.idUsuario = +this.activatedRoute.snapshot.paramMap.get('id');
  }

  ngOnInit() {
    this.getUsuario();
  }

  private getUsuario() {
    if (this.idUsuario !== 0) {
    this.usuarioService.GetUsuario(this.idUsuario).subscribe((data: Usuario) => {
      this.usuario = data;
      this.usuario.avatar = '../../assets/images/' + this.usuario.avatar;
    });
  }
}

  public entrar() {
    if (this.contrsena !== this.usuario.contrasena || this.usuario.nombre !== this.nombre) {
      Swal.fire({
        type: 'error',
        title: 'Credencialaes incorrectas',
        text: 'Comprueba que has introducido bien tus credenciales',
        showConfirmButton: false,
        timer: 1000
      });
    } else {
      Swal.fire({
        type: 'success',
        title:  `Bienvenid@ ${this.usuario.nombre}`,
        showConfirmButton: false,
        timer: 1000
      });
      if (this.usuario.nombre === 'Administrador') {
        this.router.navigate(['/usuarios']);
      } else {
        this.router.navigate([`/usuarios/${this.usuario.id}/reuniones`]);
      }
      environment.USUARIO = this.usuario;
    }
  }

  public darDeAlta() {
    this.usuario.nombre = this.nombre;
    this.usuario.contrasena = this.contrsena;
    this.usuario.profesion = this.profesion;
    if (this.avatar !== undefined) {
      this.usuario.avatar = this.avatar;
    } else {
      this.usuario.avatar = 'default_user.jpg';
    }
    this.usuarioService.AnadirUsuario(this.usuario).subscribe(_ => {
      this.router.navigate(['/usuarios']);
      Swal.fire({
        type: 'success',
        title: 'Usuario añadido correctamente',
        showConfirmButton: false,
        timer: 1000
      });
    }, error => {
      this.usuario.avatar = '../../assets/images/' + this.usuario.avatar;
      Swal.fire({
        type: 'error',
        title: 'Formulario Incorrecto',
        text: 'Comprueba que todos los campos marcados (*) esten rellenados',
        showConfirmButton: true,
        timer: 3000
      });
    });
    }

  onFileChange(event) {
    this.avatar = event.target.files[0].name;
    this.usuario.avatar = '../../assets/images/' + this.avatar;
  }

}
