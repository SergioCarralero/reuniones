import { Component, OnInit } from '@angular/core';
import { ReunionesService } from '../../services/reuniones.service';
import { MatTableDataSource } from '@angular/material';
import { Reunion } from '../../models/reunion';
import { Ubicacion } from '../../models/ubicacion';
import { UbicacionesService } from '../../services/ubicaciones.service';
import { UsuariosService } from 'src/app/services/usuarios.service';
import { Usuario } from '../../models/usuario';
import { AssertionError } from 'assert';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-lista-reuniones',
  templateUrl: './lista-reuniones.component.html',
  styleUrls: ['./lista-reuniones.component.css']
})
export class ListaReunionesComponent implements OnInit {

  filtering = false;

  public titulo;
  public descripcion;
  public ubicacion;
  public desde;

  listaReuniones: Reunion[];
  auxReuniones: Reunion[];

  ubicaciones: Ubicacion[];

  public usuario = environment.USUARIO;

  displayedColumns = ['titulo', 'descripcion', 'creador', 'ubicacion', 'tipo', 'horaEmpiece', 'horaFin', 'acciones'];

  dataSource: MatTableDataSource<Reunion>;

  constructor(private reunionesService: ReunionesService,
              private ubicacionesService: UbicacionesService,
              private toast: ToastrService) { }


  ngOnInit() {
    this.getReuniones();
    this.getUbicaciones();
  }

  private getReuniones() {
    this.reunionesService.GetReuniones().subscribe((result: Reunion[]) => {
      this.listaReuniones = result;
      this.dataSource = new MatTableDataSource(this.listaReuniones);
    }, error => {
      // tslint:disable-next-line: max-line-length
      this.toast.error('Error de conexion comprueba que la api este arrancada y funcionando correctamente', 'Error', { timeOut: 3000, progressAnimation: 'increasing', progressBar: true });
    });
  }

  private getUbicaciones() {
    this.ubicacionesService.GetUbicaciones().subscribe((result: Ubicacion[]) => {
      this.ubicaciones = result;
    });
  }

  public eliminarReunion(index: number) {
    Swal.fire({
      title: 'Eliminar Reunión',
      text: `¿Estas seguro@ de que quieres borrar esta reunión?`,
      type: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Si'
    }).then((result) => {
      if (result.value) {
        this.reunionesService.EliminarReunion(this.listaReuniones[index].id).subscribe(_ => {
          Swal.fire({
            type: 'success',
            title: 'Reunión Eliminada',
            text: 'La reunión ha sido eliminada con exito',
            showConfirmButton: true,
            timer: 1000
          });
          this.getReuniones();
        });
      }
    });
  }

  public filtrar() {
    this.auxReuniones = this.listaReuniones;

    if (this.titulo) {
      this.auxReuniones = this.auxReuniones.filter(l => l.titulo.toUpperCase().includes(this.titulo.toString().toUpperCase()));
    }

    if (this.descripcion) {
      this.auxReuniones = this.auxReuniones.filter(l => l.descripcion.toUpperCase().includes(this.descripcion.toString().toUpperCase()));
    }

    if (this.ubicacion) {
      this.auxReuniones = this.auxReuniones.filter(l => l.ubicacion.nombreUbicacion === this.ubicacion);
    }

    if (this.desde) {
      this.auxReuniones = this.auxReuniones.filter(l => l.horaEmpiece >= this.desde);
    }

    this.dataSource = new MatTableDataSource(this.auxReuniones);
  }

}
