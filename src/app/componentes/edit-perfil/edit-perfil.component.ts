import { Component, OnInit } from '@angular/core';
import { UsuariosService } from '../../services/usuarios.service';
import { Usuario } from 'src/app/models/usuario';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-edit-perfil',
  templateUrl: './edit-perfil.component.html',
  styleUrls: ['./edit-perfil.component.css']
})
export class EditPerfilComponent implements OnInit {

  usuario: Usuario = new Usuario();

  avatar: string;

  idUsuario: number;

  constructor(private usuarioService: UsuariosService, private activatedRoute: ActivatedRoute, private router: Router) {
    this.idUsuario = +this.activatedRoute.snapshot.paramMap.get('id');
    if (environment.USUARIO.id === 0) {
      this.router.navigate(['/usuarios']);
    }
  }

  ngOnInit() {
    this.getUsuario();
    ;
  }


  private getUsuario() {
    if (this.idUsuario !== 0) {
      this.usuarioService.GetUsuario(this.idUsuario).subscribe((data: Usuario) => {
        this.usuario = data;
        this.avatar = this.usuario.avatar;
        this.usuario.avatar = '../../assets/images/' + this.usuario.avatar;
      });
    }
  }

  public actualizarUsuario() {
    if (this.usuario.contrasena === '') {
      this.usuario.contrasena = null;
    }
    this.usuario.avatar = this.avatar;
    this.usuarioService.ActualizarUsuario(this.usuario.id, this.usuario).subscribe(_ => {
      this.usuario.avatar = '../../assets/images/' + this.usuario.avatar;
      Swal.fire({
        type: 'success',
        title: 'Usuario actualizado correctamente',
        showConfirmButton: false,
        timer: 1000
      });
      }, error => {
        this.usuario.avatar = '../../assets/images/' + this.usuario.avatar;
        Swal.fire({
          type: 'error',
          title: 'Formulario Incorrecto',
          text: 'Comprueba que todos los campos marcados (*) esten rellenados',
          showConfirmButton: true,
          timer: 3000
        });
    });
  }

  onFileChange(event) {
    this.avatar = event.target.files[0].name;
    this.usuario.avatar = '../../assets/images/' + this.avatar;
  }
}
