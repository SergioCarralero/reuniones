import { Component, OnInit } from '@angular/core';
import { ReunionesService } from '../../services/reuniones.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Reunion } from '../../models/reunion';
import { Usuario } from '../../models/usuario';
import { UsuariosService } from '../../services/usuarios.service';
import Swal from 'sweetalert2';
import { Ubicacion } from '../../models/ubicacion';
import { UbicacionesService } from '../../services/ubicaciones.service';
import { TiposService } from '../../services/tipos.service';
import { Tipo } from '../../models/tipo';
import { ToastrService } from 'ngx-toastr';
import { MatTableDataSource } from '@angular/material';
import { environment } from '../../../environments/environment';

declare var $: any;


@Component({
  selector: 'app-reuniones-usuario',
  templateUrl: './reuniones-usuario.component.html',
  styleUrls: ['./reuniones-usuario.component.css']
})
export class ReunionesUsuarioComponent implements OnInit {

  listaReuniones: Reunion[];
  auxReuniones: Reunion[];
  reuniones: Reunion[];
  reunionesALasQueVoy: Reunion[] = [];
  editReunion: Reunion = new Reunion();

  asistentes: Usuario[] = [];
  asistentesModal: Usuario[] = [];
  usuarios: Usuario[];
  usuariosAux: Usuario[];
  usuario: Usuario;

  ubicaciones: Ubicacion[];

  tipos: Tipo[];

  idUsuario: number;
  indexAsistente: number;

  titulo;
  descripcion;
  ubicacion;
  desde;

  filtering = false;

  loadingAsistentes: boolean;
  sinActualizar: boolean;

  dataSource: MatTableDataSource<Reunion>;


  displayedColumns = ['titulo',
    'descripcion',
    'ubicacion',
    'horaEmpiece',
    'horaFin',
    'asistentes',
    'tipo',
    'fechaCreacion',
    'fechaActualizacion',
    'acciones'];

  constructor(private reunionesService: ReunionesService, private activatedRoute: ActivatedRoute,
              private usauriosService: UsuariosService, private ubicacionesService: UbicacionesService,
              private tiposService: TiposService, private toast: ToastrService,
              private router: Router) {
    this.idUsuario = +this.activatedRoute.snapshot.paramMap.get('id');
    if (environment.USUARIO.id === 0) {
      this.router.navigate(['/usuarios']);
    }
  }

  async ngOnInit() {
    await this.getUsuario();
    this.getUsuarios();
    this.getUbicaciones();
    this.getTipos();
    this.getReunionesUsuario();
  }

  public getReunionesUsuario() {
    this.reunionesService.GetReunionesUsuario(this.idUsuario).subscribe((result: Reunion[]) => {
      this.listaReuniones = result;
      this.dataSource = new MatTableDataSource(this.listaReuniones);
      if (result.length === 0) {
        // tslint:disable-next-line: max-line-length
        this.toast.info('No tienes ninguna reunión creada, puedes crear una pulsano del botón', 'Sin reuniones', { timeOut: 3000, progressAnimation: 'increasing', progressBar: true });
      } else {
        this.reunionesALasQueVoy = [];
      }
    });
  }

  public getReunionesALasQueVoy() {
    this.reunionesService.GetReuniones().subscribe((result: Reunion[]) => {
      result.forEach(r => {
        r.asistentes.forEach(ra => {
          if (this.usuario.id === ra) {
            this.reunionesALasQueVoy.push(r);
          }
        });
      });
      if (this.reunionesALasQueVoy.length === 0) {
        // tslint:disable-next-line: max-line-length
        this.toast.info('No tienes niguna invitación', 'Sin invitaciones', { timeOut: 3000, progressAnimation: 'increasing', progressBar: true });
        this.getReunionesUsuario();
      } else {
        this.listaReuniones = [];
        this.dataSource = new MatTableDataSource(this.reunionesALasQueVoy);
      }
    });
  }

  private async getUsuario() {
    await this.usauriosService.GetUsuario(this.idUsuario).subscribe(result => {
      this.usuario = result;
    });
  }

  private getUsuarios() {
    this.usauriosService.GetUsuariosList().subscribe((result: Usuario[]) => {
      this.usuarios = result;
    });
  }

  private getAsistentes() {
    this.loadingAsistentes = true;
    this.asistentes = [];
    if (this.editReunion.asistentes.length === 0) {
      this.loadingAsistentes = false;
    } else {
      this.editReunion.asistentes.forEach((element, index) => {
        this.usauriosService.GetUsuario(element).subscribe((result: Usuario) => {
          this.asistentes.push(result);
          if (index === this.editReunion.asistentes.length - 1) {
            this.loadingAsistentes = false;
          }
        });
      });
    }
  }

  private getUbicaciones() {
    this.ubicacionesService.GetUbicaciones().subscribe((result: Ubicacion[]) => {
      this.ubicaciones = result;
    });
  }

  private getTipos() {
    this.tiposService.ObtenerTipos().subscribe((result: Tipo[]) => {
      this.tipos = result;
    });
  }

  public abrirPosteo() {
    this.asistentes = [];
    this.editReunion = new Reunion();
    this.getAsistentes();
    this.comprobarFechaActualizacion();
    this.editReunion.creadorId = this.idUsuario;
    this.editReunion.creador = this.usuario.nombre;
  }

  public anadirReunion() {
    console.log(this.editReunion);
    this.reunionesService.AnadirReunion(this.editReunion).subscribe(_ => {
      $('#modal').toggle();
      $('#modal').modal('hide');
      this.getReunionesUsuario();
      Swal.fire({
        type: 'success',
        title: 'Reunión Añadida',
        text: 'La Reunión has sido añadida con exito',
        showConfirmButton: true,
        timer: 1000
      });
    }, error => {
      if (this.editReunion.horaEmpiece > this.editReunion.horaFin) {
        Swal.fire({
          type: 'error',
          title: 'Error',
          text: 'La fecha de inicio no puede ser mayor a la fecha de fin',
          showConfirmButton: true,
        });
      } else {
        Swal.fire({
          type: 'error',
          title: 'Error',
          text: 'Asegurate de que todos los campos marcados(*) esten rellenados',
          showConfirmButton: true,
        });
      }
    });
  }

  public editar(index?: number) {
    this.editReunion = this.listaReuniones[index];
    this.getAsistentes();
    this.comprobarFechaActualizacion();
  }

  private comprobarFechaActualizacion() {
    // tslint:disable-next-line: max-line-length
    if (this.editReunion.fechaActualizacion.toString() === 'Mon Jan 01 0001 00:00:00 GMT-0014 (hora estándar de Europa central)' || this.editReunion.fechaActualizacion.toString() === '0001-01-01T00:14:44') {
      this.sinActualizar = true;
    } else {
      this.sinActualizar = false;
    }
  }

  public eliminarAsistente(index: number) {
    Swal.fire({
      title: 'Eliminar Regsitro',
      // tslint:disable-next-line: max-line-length
      text: `${this.usuario.nombre.substr(0, this.usuario.nombre.indexOf(' '))}, ¿Estas segur@ de que quieres borrar este asistente?`,
      type: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Si'
    }).then((result) => {
      if (result.value) {
        this.editReunion.asistentes.splice(index, 1);
        this.asistentes.splice(index, 1);
        if (this.editReunion.id !== 0) {
          this.reunionesService.EliminarAsistente(this.editReunion.id, this.asistentes[index].id).subscribe(_ => {
            this.asistentes.splice(index, 1);
          }, error => {
          });
        }
      }
    });
  }

  public cancelarInvitacion(index: number) {
    Swal.fire({
      title: 'Eliminar Regsitro',
      // tslint:disable-next-line: max-line-length
      text: `${this.usuario.nombre.substr(0, this.usuario.nombre.indexOf(' '))}, ¿Estas segur@ de que quieres quieres cancelar la invitacion a esta reunión?`,
      type: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Si'
    }).then((result) => {
      if (result.value) {
        this.reunionesService.EliminarAsistente(this.reunionesALasQueVoy[index].id, this.usuario.id).subscribe(_ => {
          this.reunionesALasQueVoy = [];
          this.getReunionesALasQueVoy();
        });
      }
    });
  }

  public eliminarReunion(index: number) {
    Swal.fire({
      title: 'Eliminar Regsitro',
      // tslint:disable-next-line: max-line-length
      text: `${this.usuario.nombre.substr(0, this.usuario.nombre.indexOf(' '))}, ¿Estas segur@ de que quieres borrar esta reunión?`,
      type: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Si'
    }).then((result) => {
      if (result.value) {
        this.reunionesService.EliminarReunion(this.listaReuniones[index].id).subscribe(_ => {
          this.getReunionesUsuario();
          Swal.fire({
            type: 'success',
            title: 'Reunión Borrada',
            text: 'La Reunión ha sido eliminada con exito',
            showConfirmButton: true,
            timer: 1000
          });
        }, error => {
        });
      }
    });
  }

  public actualizarReunion() {
    this.editReunion.tipo = null;
    this.editReunion.ubicacion = null;
    this.reunionesService.ActualizarReunion(this.editReunion.id, this.editReunion).subscribe(_ => {
      $('#modal').toggle();
      $('#modal').modal('hide');
      this.getReunionesUsuario();
    }, error => {
    });

    this.reunionesService.ActulizarAsistentesReunion(this.editReunion.id, this.editReunion).subscribe(_ => {
      Swal.fire({
        type: 'success',
        title: 'Reunión Actualizada',
        showConfirmButton: true,
        timer: 1000
      });
    }, error => {
      Swal.fire({
        type: 'error',
        title: error.error.horaFin[0],
        showConfirmButton: true,
      });
    });
  }

  public filtrarAsistentes() {
    this.usuariosAux = this.usuarios;

    this.editReunion.asistentes.forEach(a => {
      this.usuariosAux = this.usuariosAux.filter(ua => ua.id !== a.Id);
    });
  }

  public anadirAsistente() {
    this.editReunion.asistentes.push(this.usuariosAux[this.indexAsistente].id);
    this.asistentes.push(this.usuariosAux[this.indexAsistente]);
  }

  public filtrar() {
    this.auxReuniones = this.listaReuniones;

    if (this.titulo) {
      this.auxReuniones = this.auxReuniones.filter(l => l.titulo.toUpperCase().includes(this.titulo.toString().toUpperCase()));
    }

    if (this.descripcion) {
      this.auxReuniones = this.auxReuniones.filter(l => l.descripcion.toUpperCase().includes(this.descripcion.toString().toUpperCase()));
    }

    if (this.ubicacion) {
      this.auxReuniones = this.auxReuniones.filter(l => l.ubicacion.nombreUbicacion === this.ubicacion);
    }

    if (this.desde) {
      this.auxReuniones = this.auxReuniones.filter(l => l.horaEmpiece >= this.desde);
    }

    this.dataSource = new MatTableDataSource(this.auxReuniones);
  }

}
