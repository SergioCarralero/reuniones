import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReunionesUsuarioComponent } from './reuniones-usuario.component';

describe('ReunionesUsuarioComponent', () => {
  let component: ReunionesUsuarioComponent;
  let fixture: ComponentFixture<ReunionesUsuarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReunionesUsuarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReunionesUsuarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
