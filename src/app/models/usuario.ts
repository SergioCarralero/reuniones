export class Usuario {
    id: number;
    nombre: string;
    contrasena: string;
    avatar: string;
    profesion: string;
    reunionesCreadas: number;

    constructor() {
    this.id = 0;
    this.nombre = '';
    this.contrasena = '';
    this.avatar = '../../assets/images/default_user.jpg';
    this.profesion = '';
    this.reunionesCreadas = 0;
    }
 }
