import { Ubicacion } from './ubicacion';
import { Tipo } from './tipo';
export class Reunion {
    id: number;
    titulo: string;
    descripcion: string;
    horaEmpiece: Date;
    horaFin: Date;
    ubicacionId: number;
    ubicacion: Ubicacion;
    tipoId: number;
    tipo: Tipo;
    estado: string;
    fechaCreacion: Date;
    fechaActualizacion: Date;
    creador: string;
    creadorId: number;
    asistentes: any[];

    constructor() {
        this.id = 0;
        this.titulo = null;
        this.descripcion = null;
        this.horaEmpiece = new Date(''); // Invalid Date;
        this.horaFin = new Date(''); // Invalid Date;
        this.ubicacionId = null;
        this.tipoId = null;
        this.estado = 'valid';
        this.fechaCreacion = new Date();
        this.fechaActualizacion = new Date('0001-01-01T00:00:00');
        this.creador = '';
        this.asistentes = [];
    }
}
