import { Reunion } from './reunion';
export class Tipo {
    id: number;
    nombreTipo: string;
    reuniones: Reunion[];

    constructor() {
    this.id = 0;
    this.nombreTipo = '';
    }
}
