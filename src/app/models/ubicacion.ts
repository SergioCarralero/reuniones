import { Reunion } from './reunion';
export class Ubicacion {
    id: number;
    nombreUbicacion: string;
    reuniones: Reunion[];

    constructor() {
        this.id = 0;
        this.nombreUbicacion = '';
    }
}
