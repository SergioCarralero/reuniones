import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Reunion } from '../models/reunion';

@Injectable({
  providedIn: 'root'
})
export class ReunionesService {

  constructor(private http: HttpClient) { }

  public GetReuniones(): Observable<Reunion[]> {
    return this.http.get<any[]>(`http://localhost:51020/api/reuniones`);
    }

  public GetReunionesUsuario(id: number): Observable<Reunion[]> {
  return this.http.get<any[]>(`http://localhost:51020/api/usuarios/${id}/reuniones`);
  }

  public GetReunionDetalle(id: number): Observable<Reunion> {
  return this.http.get<Reunion>(`http://localhost:51020/api/reuniones/${id}/detalles`);
  }

  public AnadirReunion(reunion: Reunion) {
    return this.http.post<Reunion>(`http://localhost:51020/api/reuniones`, reunion);
  }

  public ActualizarReunion(id: number, reunion: Reunion) {
    return this.http.put<Reunion>(`http://localhost:51020/api/reuniones/${id}`, reunion);
  }

  public ActulizarAsistentesReunion(id: number, reunion: Reunion) {
    return this.http.put<Reunion>(`http://localhost:51020/api/reuniones/${id}/asistentes`, reunion);
  }

  public EliminarReunion(id: number) {
    return this.http.delete(`http://localhost:51020/api/reuniones/${id}`);
  }

  public EliminarAsistente(idReunion: number, idAsistente: number) {
    return this.http.delete(`http://localhost:51020/api/reuniones/${idReunion}/eliminarasistente/${idAsistente}`);
  }
}
