import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Ubicacion } from '../models/ubicacion';

@Injectable({
  providedIn: 'root'
})
export class UbicacionesService {

  constructor(private http: HttpClient) { }

  public GetUbicaciones() {
    return this.http.get<Ubicacion[]>('http://localhost:51020/api/ubicaciones');
  }

  public GetUbicacion(id: number) {
    return this.http.get<Ubicacion>(`http://localhost:51020/api/ubicaciones/${id}`);
  }

  public PostUbicacion(ubicacion: Ubicacion) {
    return this.http.post('http://localhost:51020/api/ubicaciones', ubicacion);
  }

  public PutUbicacion(id: number, ubicacion: Ubicacion) {
    return this.http.put(`http://localhost:51020/api/ubicaciones/${id}`, ubicacion);
  }

  public DeleteUbicacion(id: number) {
    return this.http.delete(`http://localhost:51020/api/ubicaciones/${id}`);
  }
}
