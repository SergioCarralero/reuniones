import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Usuario } from '../models/usuario';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {

  constructor(private http: HttpClient) { }

  public GetUsuariosList(): Observable<Usuario[]> {
    return this.http.get<Usuario[]>('http://localhost:51020/api/usuarios');
  }

  public GetUsuario(id: number): Observable<Usuario> {
    return this.http.get<Usuario>(`http://localhost:51020/api/usuarios/${id}`);
  }

  public AnadirUsuario(usuario: Usuario) {
    return this.http.post<Usuario>(`http://localhost:51020/api/usuarios`, usuario);
  }

  public ActualizarUsuario(id: number, usuario: Usuario) {
    return this.http.put<Usuario>(`http://localhost:51020/api/usuarios/${id}`, usuario);
  }

  public EliminarUsuario(id: number ) {
    return this.http.delete(`http://localhost:51020/api/usuarios/${id}`);
  }
}
