import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Tipo } from '../models/tipo';

@Injectable({
  providedIn: 'root'
})
export class TiposService {

  constructor(private http: HttpClient) { }

  public ObtenerTipos() {
    return this.http.get<Tipo[]>('http://localhost:51020/api/tipos');
  }

  public GetTipo(id: number) {
    return this.http.get<Tipo>(`http://localhost:51020/api/tipos/${id}`);
  }

  public AnadirTipo(tipo: Tipo) {
    return this.http.post('http://localhost:51020/api/tipos', tipo);

  }

  public ActualizarTipo(id: number, tipo: Tipo) {
    return this.http.put(`http://localhost:51020/api/tipos/${id}`, tipo);
  }

  public EliminarTipo(id: number) {
    return this.http.delete(`http://localhost:51020/api/tipos/${id}`);

  }
}
