import { Component, OnInit } from '@angular/core';
import { UbicacionesService } from '../../services/ubicaciones.service';
import { Ubicacion } from '../../models/ubicacion';
import Swal from 'sweetalert2';
import { MatTableDataSource } from '@angular/material';

declare var $: any;

@Component({
  selector: 'app-ubicaciones',
  templateUrl: './ubicaciones.component.html',
  styleUrls: ['./ubicaciones.component.css']
})
export class UbicacionesComponent implements OnInit {

  ubicaciones: Ubicacion[];

  editUbicacion: Ubicacion = new Ubicacion();

  dataSource: MatTableDataSource<Ubicacion>;

  displayedColumns = ['nombre', 'acciones'];

  constructor(private ubicacionesService: UbicacionesService) { }

  ngOnInit() {
    this.getUbicaciones();
  }

  private getUbicaciones() {
    this.ubicacionesService.GetUbicaciones().subscribe((result: Ubicacion[]) => {
      this.ubicaciones = result;
      this.dataSource = new MatTableDataSource(this.ubicaciones);
    });
  }

  public editarUbicacion(index?: number) {
    if (index !== undefined) {
      this.editUbicacion = this.ubicaciones[index];
    } else {
      this.editUbicacion = new Ubicacion();
    }
  }

  public actualizarUbicacion() {
    this.editUbicacion.reuniones = [];
    this.ubicacionesService.PutUbicacion(this.editUbicacion.id, this.editUbicacion).subscribe(_ => {
      $('#exampleModal').modal('toggle');
      $('#exampleModal').modal('hide');
      Swal.fire({
        type: 'success',
        title: 'Ubicación Actualizada',
        showConfirmButton: true,
        timer: 1000
      });
      this.getUbicaciones();
    });
  }

  public anadirUbicacion() {
    this.ubicacionesService.PostUbicacion(this.editUbicacion).subscribe(_ => {
      $('#exampleModal').modal('toggle');
      $('#exampleModal').modal('hide');
      Swal.fire({
        type: 'success',
        title: 'Ubicación Añadida',
        showConfirmButton: true,
        timer: 1000
      });
      this.getUbicaciones();
    });
  }

  public eliminarUbicacion(index: number) {
    Swal.fire({
      title: 'Eliminar Regsitro',
      text: `¿Estas segur@ de que quieres borrar esta ubicación?`,
      type: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Si'
    }).then((result) => {
      if (result.value) {
        this.ubicacionesService.DeleteUbicacion(this.ubicaciones[index].id).subscribe(_ => {
          this.getUbicaciones();
          Swal.fire({
            type: 'success',
            title: 'Tipo Borrado',
            text: 'El Tipo ha sido eliminad con exito',
            showConfirmButton: true,
            timer: 1000
          });
        });
      }
    });
  }

}
