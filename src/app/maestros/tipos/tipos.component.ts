import { Component, OnInit } from '@angular/core';
import { TiposService } from '../../services/tipos.service';
import { Tipo } from '../../models/tipo';
import { MatTableDataSource } from '@angular/material';
import Swal from 'sweetalert2';
import { Reunion } from '../../models/reunion';
import { ReunionesService } from '../../services/reuniones.service';

declare var $: any;

@Component({
  selector: 'app-tipos',
  templateUrl: './tipos.component.html',
  styleUrls: ['./tipos.component.css']
})
export class TiposComponent implements OnInit {

  constructor(private tipoService: TiposService, private reunionesService: ReunionesService) { }

  tipos: Tipo[];
  reuniones: Reunion[];

  editTipo: Tipo = new Tipo();

  dataSource: MatTableDataSource<Tipo>;

  displayedColumns = ['nombre', 'acciones'];

  ngOnInit() {
    this.getTipos();
  }

  private getTipos() {
    this.tipoService.ObtenerTipos().subscribe((result: Tipo[]) => {
      this.tipos = result;
      this.dataSource = new MatTableDataSource(this.tipos);
    });
  }

  public editarTipo(index?: number) {
    if (index !== undefined) {
      this.editTipo = this.tipos[index];
    } else {
      this.editTipo = new Tipo();
    }
  }

  public actualizarTipo() {
    this.editTipo.reuniones = [];
    this.tipoService.ActualizarTipo(this.editTipo.id, this.editTipo).subscribe(_ => {
      $('#exampleModal').modal('toggle');
      $('#exampleModal').modal('hide');
      Swal.fire({
        type: 'success',
        title: 'Tipo Actualizado',
        showConfirmButton: true,
        timer: 1000
      });
      this.getTipos();
    });
  }

  public anadirTipo() {
    this.tipoService.AnadirTipo(this.editTipo).subscribe(_ => {
      $('#exampleModal').modal('toggle');
      $('#exampleModal').modal('hide');
      Swal.fire({
        type: 'success',
        title: 'Tipo Añadido',
        showConfirmButton: true,
        timer: 1000
      });
      this.getTipos();
    });
  }

  public eliminarTipo(index: number) {
    Swal.fire({
      title: 'Eliminar Regsitro',
      text: `¿Estas segur@ de que quieres borrar este tipo?`,
      type: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Si'
    }).then((result) => {
      if (result.value) {
        this.tipoService.EliminarTipo(this.tipos[index].id).subscribe(_ => {
          this.getTipos();
          Swal.fire({
            type: 'success',
            title: 'Tipo Borrado',
            text: 'El Tipo ha sido eliminad con exito',
            showConfirmButton: true,
            timer: 1000
          });
        });
      }
    });
  }
}
