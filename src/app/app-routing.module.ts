import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetallesUsuarioComponent } from './componentes/detalles-usuario/detalles-usuario.component';
import { PageNotFoundComponent } from './errors/page-not-found/page-not-found.component';
import { ListaUsuariosComponent } from './componentes/lista-usuarios/lista-usuarios.component';
import { ListaReunionesComponent } from './componentes/lista-reuniones/lista-reuniones.component';
import { ReunionesUsuarioComponent } from './componentes/reuniones-usuario/reuniones-usuario.component';
import { HomeComponent } from './componentes/home/home.component';
import { EditPerfilComponent } from './componentes/edit-perfil/edit-perfil.component';
import { TiposComponent } from './maestros/tipos/tipos.component';
import { UbicacionesComponent } from './maestros/ubicaciones/ubicaciones.component';

const routes: Routes = [

  {path: 'usuarios', component: ListaUsuariosComponent},
  {path: 'reuniones', component: ListaReunionesComponent},
  {path: 'usuarios/:id/reuniones', component: ReunionesUsuarioComponent},
  {path: 'usuarios/:id/detalles', component: DetallesUsuarioComponent},
  {path: 'usuarios/alta', component: DetallesUsuarioComponent},
  {path: 'usuarios/:id/editarperfil', component: EditPerfilComponent},
  {path: 'home', component: HomeComponent},


  {path: 'tipos', component: TiposComponent},
  {path: 'ubicaciones', component: UbicacionesComponent},


  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: '**', redirectTo: '/error'},
  {path: 'error', component: PageNotFoundComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
