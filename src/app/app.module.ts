import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { DetallesUsuarioComponent } from './componentes/detalles-usuario/detalles-usuario.component';
import { PageNotFoundComponent } from './errors/page-not-found/page-not-found.component';
import { ListaUsuariosComponent } from './componentes/lista-usuarios/lista-usuarios.component';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ListaReunionesComponent } from './componentes/lista-reuniones/lista-reuniones.component';
import { ReunionesUsuarioComponent } from './componentes/reuniones-usuario/reuniones-usuario.component';
import { HomeComponent } from './componentes/home/home.component';
import { ToastrModule } from 'ngx-toastr';
import { EditPerfilComponent } from './componentes/edit-perfil/edit-perfil.component';

// Material
import { MatFormFieldModule } from '@angular/material/form-field';
// tslint:disable-next-line: max-line-length
import { MatInputModule, MatPaginatorModule, MatSortModule, MatSliderModule, MatMenuModule, MatIconModule, MatAutocompleteModule, MatNativeDateModule } from '@angular/material';
import { MatDatepickerModule  } from '@angular/material/datepicker';
import { MatRadioModule } from '@angular/material/radio';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { MatTableModule } from '@angular/material/table';
import { MatSelectModule } from '@angular/material/select';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatExpansionModule } from '@angular/material/expansion';
import { TiposComponent } from './maestros/tipos/tipos.component';
import { UbicacionesComponent } from './maestros/ubicaciones/ubicaciones.component';
import { NavbarComponent } from './componentes/layout/navbar/navbar.component';



@NgModule({
  declarations: [
    AppComponent,
    DetallesUsuarioComponent,
    PageNotFoundComponent,
    ListaUsuariosComponent,
    ListaReunionesComponent,
    ReunionesUsuarioComponent,
    HomeComponent,
    EditPerfilComponent,
    TiposComponent,
    UbicacionesComponent,
    NavbarComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),

    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatRadioModule,
    MatCardModule,
    MatButtonModule,
    MatDividerModule,
    MatListModule,
    MatTableModule,
    MatSelectModule,
    MatCheckboxModule,
    MatTabsModule,
    MatPaginatorModule,
    MatSortModule,
    MatTooltipModule,
    MatButtonToggleModule,
    MatExpansionModule,
    MatSliderModule,
    MatMenuModule,
    MatIconModule,
    MatAutocompleteModule,
    MatNativeDateModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
